//
//  JSONConverter.swift
//  AssesmentAltran
//
//  Created by Aleix Cos Pous on 30/1/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

import Foundation

class JSONConverter {
  enum ConverterError: Error {
    case InvalidFormat
    case InvalidConversion
    
    func printError() -> String {
      switch self {
      case .InvalidFormat:
        return "There was an error converting the JSON to a Dictionary"
      case .InvalidConversion:
        return "There was an error converting the Dictionary to an Array"
      }
    }
  }
  
  static func toArray(fromJSON JSON: Any, key:String) throws -> [[String:AnyObject]]{
    guard let dict = JSON as? NSDictionary else {
      throw ConverterError.InvalidFormat
    }
    guard let array = dict.value(forKey: key) as? [[String:AnyObject]] else {
      throw ConverterError.InvalidConversion
    }
    return array
  }

}
