//
//  ShowAlert.swift
//  AssesmentAltran
//
//  Created by Aleix Cos Pous on 1/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

import Foundation
import UIKit

class ShowAlert {
  static func alert(title: String?, message: String?, style: UIAlertControllerStyle = .alert, viewController: UIViewController){
    let alert = UIAlertController(title: title, message: message, preferredStyle: style)
    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
    viewController.present(alert, animated: true, completion: nil)
  }
}
