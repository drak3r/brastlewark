//
//  ListOfGnomesViewController.swift
//  AssesmentAltran
//
//  Created by Aleix Cos Pous on 30/1/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import CoreData

class ListOfGnomesViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet weak var labelDownloadingGnomes: UILabel!
  
  lazy var arrayOfGnomes = [Gnome]()
  lazy var filteredGnomes = [Gnome]()
  let searchController = UISearchController(searchResultsController: nil)
  var filteredProfession: String?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    title = "Brastlewark"
    tableView.tableFooterView = UIView()
    getGnomesFromCoreData()
    searchController.searchResultsUpdater = self
    searchController.searchBar.delegate = self
    searchController.searchBar.scopeButtonTitles = ["Name", "Hair Color"]
    searchController.dimsBackgroundDuringPresentation = false
    definesPresentationContext = true
    searchController.searchBar.barTintColor = .white
    tableView.tableHeaderView = searchController.searchBar
  }
  
  // MARK: - Core Data & Network Methods
  func getGnomesFromCoreData(){
    do{
      arrayOfGnomes = try GnomeManager.fetchGnoms()
      if(arrayOfGnomes.count == 0){
        // Core Data has no data, retrieve from Internet
        requestGnomes()
      } else {
        // Core Data has data
        tableView.reloadData()
      }
    } catch let error {
      ShowAlert.alert(title: nil, message: error.localizedDescription, viewController: self)
    }
  }
  
  func requestGnomes(){
    hideViews(hide: false)
    NetworkOperation.requestGnomes(target: self) { gnomes, error in
      if (error == nil){
        // There is no error downloading the Gnomes
        self.arrayOfGnomes = gnomes!
        self.tableView.reloadData()
        self.hideViews(hide: true)
      } else {
        self.activityIndicator.stopAnimating()
        self.labelDownloadingGnomes.text = "There was an error downloading the data"
      }
    }
  }
  
  // MARK: - UI
  func hideViews(hide: Bool){
    labelDownloadingGnomes.isHidden = hide
    navigationItem.leftBarButtonItem?.isEnabled = hide
     hide == true ? activityIndicator.stopAnimating() : activityIndicator.startAnimating()
  }
  
  // MARK: - Filter Methods
  func filterContentForText(text: String, scope: String = "Name"){
    if text.characters.count != 0 {
      filteredGnomes = arrayOfGnomes.filter { gnome in
        if(scope == "Hair Color"){
          if filteredProfession != nil {
            // Put in the filtered array gnomes of profession selected and hair color the user is searching for
            return gnome.hairColor.lowercased().contains(text.lowercased()) && gnome.professions.contains(filteredProfession!)
          }else {
            // Put in the filtered array gnomes of the hair color desired
            return gnome.hairColor.lowercased().contains(text.lowercased())
          }
        } else {
          if filteredProfession != nil {
            // Put in the filtered array gnomes that has the name the user is searching and filtered by profession
            return gnome.name.lowercased().contains(text.lowercased()) && gnome.professions.contains(filteredProfession!)
          }else {
            // Put in the filtered array gnomes that has the name
            return gnome.name.lowercased().contains(text.lowercased())
          }
        }
      }
    }
    tableView.reloadData()
  }
  
  func filterContentForProfession(profession: String){
    if profession != "All"{
      filteredGnomes = arrayOfGnomes.filter { gnome in
        return gnome.professions.contains(profession)
      }
    }
    tableView.reloadData()
  }
  
  // MARK: - Navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "showDetail" {
      if let indexPath = tableView.indexPathForSelectedRow {
        let gnome: Gnome
        if (searchController.isActive && searchController.searchBar.text != "") || self.filteredProfession != nil {
          gnome = filteredGnomes[indexPath.row]
        } else {
          gnome = arrayOfGnomes[indexPath.row]
        }
        let detailVC = segue.destination as! GnomeDetailViewController
        detailVC.gnome = gnome
      }
    } else if segue.identifier == "filterProfessions"{
     let filterProfessionsVC = (segue.destination as! UINavigationController).topViewController as! FilterProfessionsTableViewController
      filterProfessionsVC.delegate = self
      if let filteredProfession = filteredProfession{
        filterProfessionsVC.selectedProfession = filteredProfession
      }
    }
  }
}

// MARK: - Table View Data Source
extension ListOfGnomesViewController: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if (searchController.isActive && searchController.searchBar.text != "") || filteredProfession != nil{
      return filteredGnomes.count
    }
    return arrayOfGnomes.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "gnomeCell", for: indexPath) as! GnomeTableViewCell
    let gnome: Gnome
    
    if (searchController.isActive && searchController.searchBar.text != "") || filteredProfession != nil  {
      gnome = filteredGnomes[indexPath.row]
    } else {
      gnome = arrayOfGnomes[indexPath.row]
    }
    cell.labelName.text = gnome.name
    cell.labelAge.text = "\(gnome.age)"
    var professions: String = ""
    for (index, profession) in gnome.professions.enumerated(){
      if index == 0{
        professions += profession
      } else {
        professions += ", \(profession)"
      }
    }
    cell.labelProfession.text = gnome.professions.count != 0 ? professions : "Doesn't work"
    let urlImage = URL(string: gnome.thumbnailURL)!
    cell.imageGnome.af_setImage(withURL: urlImage, placeholderImage: #imageLiteral(resourceName: "gnome"))
    cell.viewHairColor.backgroundColor = gnome.getHairColor()
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 70
  }

}
// MARK: - Search Results & Search Bar Delegate

extension ListOfGnomesViewController: UISearchResultsUpdating, UISearchBarDelegate {
  func updateSearchResults(for searchController: UISearchController) {
    let searchBar = searchController.searchBar
    let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
    filterContentForText(text: searchController.searchBar.text!, scope: scope)
  }
  
  func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    filterContentForText(text: searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
  }
}
// MARK: - Filter Delegate

extension ListOfGnomesViewController: FilterProfessionsDelegate{
  func didSelectProfession(profession: String) {
    self.filteredProfession = (profession == "All" ? nil : profession)
    filterContentForProfession(profession: profession)
  }
}
