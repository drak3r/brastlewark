//
//  FilterProfessionsTableViewController.swift
//  AssesmentAltran
//
//  Created by Aleix Cos Pous on 31/1/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

import UIKit

protocol FilterProfessionsDelegate {
  func didSelectProfession(profession: String)
}

class FilterProfessionsTableViewController: UITableViewController {
  var professions = [String]()
  var selectedProfession: String?
  var delegate: FilterProfessionsDelegate?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    var arrayProfessions = [String]()
    do{
    let gnomes = try GnomeManager.fetchGnoms()
      for gnome in gnomes {
        for profession in gnome.professions{
          let formattedProfession = profession.replacingOccurrences(of: " ", with: "")
          if !arrayProfessions.contains(formattedProfession){
            arrayProfessions.append(formattedProfession)
          } else {
            break
          }
        }
      }
      professions = arrayProfessions.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
      professions.insert("All", at: 0)
      tableView.reloadData()
    }catch let error {
      ShowAlert.alert(title: nil, message: error.localizedDescription, viewController: self)
    }
  }

  // MARK: - Table view data source

  override func numberOfSections(in tableView: UITableView) -> Int {
      return 1
  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return professions.count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "professionCell", for: indexPath) as! ProfessionTableViewCell
      let profession = professions[indexPath.row]
      cell.labelProfession.text = profession
      cell.labelProfession.textColor = .black
      cell.checkIcon.isHidden = true

      if let selectedProfession = selectedProfession {
        if selectedProfession == profession {
          cell.labelProfession.textColor = UIColor(red: 1, green: 0, blue: 98/255.0, alpha: 1)
          cell.checkIcon.isHidden = false
        }
      } else if (indexPath.row == 0){
        cell.labelProfession.textColor = UIColor(red: 1, green: 0, blue: 98/255.0, alpha: 1)
        cell.checkIcon.isHidden = false
    }
      return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let profession = professions[indexPath.row]
    delegate?.didSelectProfession(profession: profession)
    dismiss(animated: true, completion: nil)
  }
  
  // MARK: - IBActions

  @IBAction func dismiss(_ sender: UIBarButtonItem) {
    dismiss(animated: true, completion: nil)
  }
}
