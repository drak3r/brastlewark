//
//  GnomeDetailViewController.swift
//  AssesmentAltran
//
//  Created by Aleix Cos Pous on 30/1/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

import UIKit
import AlamofireImage
import CoreData

class GnomeDetailViewController: UIViewController {
  @IBOutlet weak var gnomeImageView: UIImageView!
  @IBOutlet weak var labelName: UILabel!
  @IBOutlet weak var labelAge: UILabel!
  @IBOutlet weak var labelWeight: UILabel!
  @IBOutlet weak var labelHeight: UILabel!
  @IBOutlet weak var labelGender: UILabel!
  @IBOutlet weak var viewHairColor: UIView!
  @IBOutlet weak var labelProfessions: UILabel!
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var labelFriendOfGnome: UILabel!

  var gnome: Gnome?
  
  override func viewDidLoad() {
      super.viewDidLoad()
      // Do any additional setup after loading the view.
    self.view.layoutIfNeeded()
    gnomeImageView.layer.cornerRadius = gnomeImageView.bounds.size.width/2.0
    gnomeImageView.layer.masksToBounds = true
    viewHairColor.layer.cornerRadius = viewHairColor.bounds.size.width/2.0
    viewHairColor.layer.masksToBounds = true
    configureView()
    
    let gesture = UITapGestureRecognizer(target: self, action: #selector(animateImage))
    gesture.numberOfTapsRequired = 1
    gnomeImageView.isUserInteractionEnabled = true
    gnomeImageView.addGestureRecognizer(gesture)
  }
  
  // MARK: - UI
  func animateImage(){
    let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
    rotation.toValue = NSNumber(value: M_PI * 2)
    rotation.duration = 1
    gnomeImageView.layer.add(rotation, forKey: "rotationAnimation")
  }
  
  func configureView(){
    if let gnome = gnome {
      title = gnome.name
      labelName.text = gnome.name
      labelFriendOfGnome.text = gnome.name
      labelAge.text = "\(gnome.age)"
      labelWeight.text = "\(gnome.weight)"
      labelHeight.text = "\(gnome.height)"
      viewHairColor.backgroundColor = gnome.getHairColor()
      labelGender.text = gnome.gender
      var professions: String = ""
      for (index, profession) in gnome.professions.enumerated(){
        if index == 0{
          professions += profession
        } else {
          professions += ", \(profession)"
        }
      }
      labelProfessions.text = (gnome.professions.count == 0 ? "Doesn't work at the moment" : professions)
      gnomeImageView.af_setImage(withURL: URL(string: gnome.thumbnailURL)!)
    }
  }
}

// MARK: - Collection View Data Source / Delegate
extension GnomeDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    if gnome?.friends.count == 0 {
      // Gnome has no friends. Show a label
      let label = UILabel(frame: CGRect(x: 0, y: 0, width: collectionView.bounds.size.width * 0.4, height: collectionView.bounds.size.height))
      label.text = "\(gnome!.name) has no friends"
      label.font = UIFont(name: "HelveticaNeue-Thin", size: 20.0)
      label.textAlignment = .center
      label.adjustsFontSizeToFitWidth = true
      collectionView.backgroundView = label
      return 0
    } else {
      collectionView.backgroundView = nil
      return 1
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return (gnome?.friends.count)!
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gnomeDetailCell", for: indexPath) as! GnomeDetailCollectionViewCell
    if let gnomeFriendName = gnome?.friends[indexPath.item]{
      do{
        let friend = try GnomeManager.getGnomeByName(friendName: gnomeFriendName)
        let formattedFriendName = gnomeFriendName.components(separatedBy: " ").first
        cell.labelName.text = formattedFriendName
        cell.gnomeImageView.af_setImage(withURL: URL(string: friend.thumbnailURL)!)
      } catch let error {
        ShowAlert.alert(title: nil, message: error.localizedDescription, viewController: self)
      }
    }
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if let gnomeFriendName = gnome?.friends[indexPath.item]{
      do{
        let friend = try GnomeManager.getGnomeByName(friendName: gnomeFriendName)
        let vc = storyboard?.instantiateViewController(withIdentifier: "GnomeDetailViewController") as! GnomeDetailViewController
        vc.gnome = friend
        navigationController?.replaceTopViewController(with: vc, animated: true)
      } catch let error {
        ShowAlert.alert(title: nil, message: error.localizedDescription, viewController: self)
      }
    }
  }
}
// MARK: - UINavigationController

extension UINavigationController{
  func replaceTopViewController(with viewController: UIViewController, animated: Bool) {
    var vcs = viewControllers
    vcs[vcs.count - 1] = viewController
    setViewControllers(vcs, animated: animated)
  }
}
