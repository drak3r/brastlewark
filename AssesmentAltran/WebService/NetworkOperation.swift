//
//  NetworkOperation.swift
//  AssesmentAltran
//
//  Created by Aleix Cos Pous on 31/1/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

import Foundation
import Alamofire

private let baseUrl = "https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json"

class NetworkOperation {
  static func requestGnomes(target: UIViewController, withCompletionHandler completion: @escaping (([Gnome]?, Error?)->())){
    Alamofire.request(baseUrl).responseJSON { (response) in
      if let JSON = response.result.value{
        do{
          let array = try JSONConverter.toArray(fromJSON: JSON, key: "Brastlewark")
          try GnomeManager.saveGnomes(fromArray: array)
          let gnomes = try GnomeManager.fetchGnoms()
          completion(gnomes, nil)
        } catch let error as GnomeManager.ManagerError {
          ShowAlert.alert(title: nil, message: error.printError(), viewController: target)
          completion(nil, error)
        } catch let error as JSONConverter.ConverterError{
          ShowAlert.alert(title: nil, message: error.printError(), viewController: target)
          completion(nil, error)
        } catch let error{
          ShowAlert.alert(title: nil, message: error.localizedDescription, viewController: target)
        }
      }
    }
  }
  
  //Native Way
  /*
  static func requestGnomes(target: UIViewController, withCompletionHandler completion: @escaping (([Gnome]?, Error?)->())){
    let task = URLSession.shared.dataTask(with: URL(string: baseUrl)!) { data, response, error in
      guard error == nil else {
        print(error)
        return
      }
      guard let data = data else {
        print("Data is empty")
        return
      }
      
      let json = try! JSONSerialization.jsonObject(with: data, options: [])
      do{
        let array = try JSONConverter.toArray(fromJSON: json, key: "Brastlewark")
        try GnomeManager.saveGnomes(fromArray: array)
        let gnomes = try GnomeManager.fetchGnoms()
        completion(gnomes, nil)
      } catch let error as GnomeManager.ManagerError {
        ShowAlert.alert(title: nil, message: error.printError(), viewController: target)
        completion(nil, error)
      } catch let error as JSONConverter.ConverterError{
        ShowAlert.alert(title: nil, message: error.printError(), viewController: target)
        completion(nil, error)
      } catch let error{
        ShowAlert.alert(title: nil, message: error.localizedDescription, viewController: target)
      }
    }
    task.resume()
  }*/
}
