//
//  Gnome.swift
//  AssesmentAltran
//
//  Created by Aleix Cos Pous on 30/1/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Gnome: NSManagedObject {
  @NSManaged var id: Int
  @NSManaged var name: String
  @NSManaged var thumbnailURL: String
  @NSManaged var age: Int
  @NSManaged var weight: Double
  @NSManaged var height: Double
  @NSManaged var hairColor: String
  @NSManaged var professions: [String]
  @NSManaged var friends: [String]
  @NSManaged var gender: String
  
  func getHairColor() -> UIColor {
    switch hairColor {
    case "Red":
      return .red
    case "Gray":
      return .gray
    case "Pink":
      return UIColor(red: 1, green: 192/255.0, blue: 203/255.0, alpha: 1)
    case "Black":
      return .black
    case "Green":
      return .green
    default:
      return .white
    }
  }
}


class GnomeManager {
  enum ManagerError: Error {
    case InvalidConversion
    case NoAppDelegate
    
    func printError() -> String {
      switch self {
      case .InvalidConversion:
        return "There was an error converting the array to the Gnome model"
      case .NoAppDelegate:
        return "There isn't an AppDelegate"
      }
    }
  }
  
  // Get the array of the JSON and save to Core data
  static func saveGnomes(fromArray array: [[String:AnyObject]]) throws {
    for gnomesDictionary in array {
      guard let id = gnomesDictionary["id"] as? Int,
        let name = gnomesDictionary["name"] as? String,
        let thumbnail = gnomesDictionary["thumbnail"] as? String,
        let age = gnomesDictionary["age"] as? Int,
        let weight = gnomesDictionary["weight"] as? Double,
        let height = gnomesDictionary["height"] as? Double,
        let hairColor = gnomesDictionary["hair_color"] as? String,
        let professions = gnomesDictionary["professions"] as? [String],
        let friends = gnomesDictionary["friends"] as? [String]
        else {
          throw ManagerError.InvalidConversion
      }
      
      let gender = (hairColor == "Pink" ? "Female" : "Male")
      do {
         try saveToCoreDataWith(id: id, name: name, thumbnailURL: thumbnail, age: age, weight: weight, height: height, hairColor: hairColor, gender: gender, professions: professions, friends: friends)
      } catch let error{
        throw error
      }
    }
  }
  
  private static func saveToCoreDataWith(id: Int, name: String, thumbnailURL: String, age: Int, weight: Double, height: Double, hairColor: String, gender: String, professions: [String], friends: [String]) throws{
    
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      throw ManagerError.NoAppDelegate
    }
    let managedContext = appDelegate.persistentContainer.viewContext
    let entity = NSEntityDescription.entity(forEntityName: "GnomeEntity", in: managedContext)!
    let gnome = NSManagedObject(entity: entity, insertInto: managedContext) as! Gnome
    
    gnome.id = id
    gnome.name = name
    gnome.thumbnailURL = thumbnailURL
    gnome.age = age
    gnome.weight = weight
    gnome.height = height
    gnome.hairColor = hairColor
    gnome.gender = gender
    gnome.professions = professions
    gnome.friends = friends
    
    do {
      try managedContext.save()
    } catch let error as NSError{
      print("Could not save. \(error), \(error.userInfo)")
      throw error
    }
  }
  
  // Fetch all the gnoms of Core Data
  static func fetchGnoms() throws -> [Gnome]{
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      throw ManagerError.NoAppDelegate
    }
    let managedContext = appDelegate.persistentContainer.viewContext
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "GnomeEntity")
    
    do {
      let gnomes = try managedContext.fetch(fetchRequest) as! [Gnome]
      return gnomes
    } catch let error as NSError {
      print("Could not fetch. \(error), \(error.userInfo)")
      throw error
    }
  }
  
  // Fetch gnome of Core Data by name
  static func getGnomeByName(friendName: String) throws -> Gnome{
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      throw ManagerError.NoAppDelegate
    }
    let managedContext = appDelegate.persistentContainer.viewContext
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "GnomeEntity")
    fetchRequest.predicate = NSPredicate(format: "name == %@", friendName)
    
    do {
      let friends = try managedContext.fetch(fetchRequest) as! [Gnome]
      return friends.first!
    } catch let error as NSError {
      print("Could not fetch. \(error), \(error.userInfo)")
      throw error
    }
  }

}
