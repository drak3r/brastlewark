//
//  GnomeDetailCollectionViewCell.swift
//  AssesmentAltran
//
//  Created by Aleix Cos Pous on 31/1/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

import UIKit

class GnomeDetailCollectionViewCell: UICollectionViewCell {
  @IBOutlet weak var gnomeImageView: UIImageView!
  @IBOutlet weak var labelName: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    layoutIfNeeded()
    gnomeImageView.layer.cornerRadius = gnomeImageView.bounds.size.width/2.0
    gnomeImageView.layer.masksToBounds = true
  }
}
