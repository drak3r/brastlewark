//
//  GnomeTableViewCell.swift
//  AssesmentAltran
//
//  Created by Aleix Cos Pous on 30/1/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

import UIKit

class GnomeTableViewCell: UITableViewCell {

  @IBOutlet weak var imageGnome: UIImageView!
  @IBOutlet weak var labelName: UILabel!
  @IBOutlet weak var labelProfession: UILabel!
  @IBOutlet weak var labelAge: UILabel!
  @IBOutlet weak var viewHairColor: UIView!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      self.layoutIfNeeded()
      imageGnome.layer.cornerRadius = imageGnome.bounds.size.width/2.0
      imageGnome.layer.masksToBounds = true
      viewHairColor.layer.cornerRadius = viewHairColor.bounds.size.width/2.0
      viewHairColor.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
