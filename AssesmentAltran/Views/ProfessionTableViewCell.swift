//
//  ProfessionTableViewCell.swift
//  AssesmentAltran
//
//  Created by Aleix Cos Pous on 31/1/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

import UIKit

class ProfessionTableViewCell: UITableViewCell {
  @IBOutlet weak var checkIcon: UIImageView!
  @IBOutlet weak var labelProfession: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
